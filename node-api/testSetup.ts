
// node-api/testSetup.ts

process.env.DB_HOST = 'localhost';
process.env.DB_PORT = '3306';
process.env.DB_USER = 'root';
process.env.DB_PASSWORD = 'mypassword';
process.env.DB_DATABASE = 'book_store';
