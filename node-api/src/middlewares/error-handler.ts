// src/middlewares/error-handler.ts

import { Request, Response, NextFunction } from 'express';
import { HttpError } from '../utils/errors';

/**
 * This global error handling middleware to check all HTTP errors and
 * It sends the error response to client with JSON format
 */
const errorHandler = (error: Error, _req: Request, res: Response, _next: NextFunction) => {
    if (error instanceof HttpError) {
        // if the error type is one of the HTTP error
        // then getting details form error and sending error response in json format
        const status = error.status || 500;
        const message = error.message || JSON.stringify({ error: 'Something went wrong' });
        const stack = error.stack || {};
        res.setHeader('Content-Type', 'application/json');
        res.status(status).json({
            message: JSON.parse(message),
            stack,
        });
        res.end();
    } else {
        // If any server related error like db connection failed, timeout etc..
        // then it simply sends 500 error
        res.setHeader('Content-Type', 'application/json');
        res.status(500).json({
            message: {
                error: 'Something went wrong! Please try after some time.',
                stack: error.stack,
            },
        });
        res.end();
    }
};

export default errorHandler;
