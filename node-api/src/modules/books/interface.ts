// src/modules/books/interface.ts

export interface IBook {
    title: string;
    author: string;
    published_on: string;
    isbn: string;
    genre: string;
    price: number;
    description: string;
    qty?: number
}