// src/modules/books/router.ts

import express, { NextFunction, Request, Response } from 'express';
import Joi from 'joi';
import { searchBooksSchema } from './params';
import { BadRequest } from '../../utils/errors';
import { getBooks } from './controller';

const router = express.Router();


const paramsValidator = (schema: Joi.Schema) => (req: Request, res: Response, next: NextFunction) => {
    let payload = {}
    switch (req.method) {
        case 'GET':
            payload = req.query;
            break;
        case 'POST':
            payload = req.body;
            break;
        default:
            payload = req.body;
            break;
    }
    try {
        const value = Joi.attempt(payload, schema, {
            abortEarly: false,
        });
        next();
    } catch (error: any) {
        console.error(error.details);
        next(new BadRequest({
            error: 'Bad Request! Please check request parameters.',
            context: error.details,
            payload,
        }));
    }
}

router.post('/', paramsValidator(searchBooksSchema), getBooks);

// router.post('/add', paramsValidator(addBookSchema), addBook);


export default router;