// src/modules/books/controller.ts

import { NextFunction, Request, Response } from 'express';
import { getConnection } from '../../utils/db'
// import { IBook } from './interface';
import { RowDataPacket } from 'mysql2';


export const searchBook = async (
    search: string,
    limit: number = 5,
    page: number = 1,
    out_of_stock: boolean = false,
    sort: string = '',
    order: string = '',
    published_from: string | null = null,
    published_to: string | null = null
): Promise<{
    totalResults: number,
    totalPages: number,
    currentPage: number,
    books: RowDataPacket[]
}> => {
    const conn = await getConnection();
    return new Promise((resolve, reject) => {
        let countQuery = 'SELECT COUNT(*) as total FROM books WHERE (title LIKE ? OR author LIKE ? OR genre LIKE ?)';
        let searchQuery = 'SELECT * FROM books WHERE (title LIKE ? OR author LIKE ? OR genre LIKE ?)';

        if (!out_of_stock) {
            countQuery += ' AND qty > 0';
            searchQuery += ' AND qty > 0';
        }

        if (published_from && published_to) {
            countQuery += ` AND published_on BETWEEN '${published_from}' AND '${published_to}'`;
            searchQuery += ` AND published_on BETWEEN '${published_from}' AND '${published_to}'`;
        }

        if (sort != '' && order != '') {
            searchQuery += ` ORDER BY ${sort} ${order}`;
        }
        searchQuery += ' LIMIT ? OFFSET ?'

        conn.query(countQuery, [`%${search}%`, `%${search}%`, `%${search}%`], (err, countResult: RowDataPacket[]) => {
            if (err) {
                console.error('Error counting search results:', err);
                reject(err);
                return;
            }
            const totalResults = countResult[0].total;
            const totalPages = Math.ceil(totalResults / limit);
            if (totalPages < page) {
                page = 1
            }
            const offset = (page - 1) * limit;

            conn.query(searchQuery, [`%${search}%`, `%${search}%`, `%${search}%`, limit, offset], (err, result: RowDataPacket[]) => {
                conn.release()
                if (err) {
                    // console.error('Error search results:', err);
                    reject(err);
                    return;
                }
                resolve({
                    totalResults: totalResults,
                    totalPages: totalPages,
                    currentPage: page,
                    books: result
                })

            });
        });

    })
}

export const getBooks = async ({ body }: Request, res: Response, next: NextFunction) => {
    try {
        const payload = {
            search: body.search || '',
            page: body.page || 1,
            out_of_stock: body.out_stock || false,
            sort: body.sort || '',
            order: body.order || '',
            published_from: body.published.from || null,
            published_to: body.published.to || null
        }

        const searchResult = await searchBook(
            payload.search, 5, payload.page,
            payload.out_of_stock, payload.sort, payload.order,
            payload.published_from, payload.published_to
        );
        const result = {
            message: 'List of Books',
            data: searchResult,
            payload: body,
        };
        res.status(200).json(result);
        res.end();
    } catch (error) {
        next(error)
    }
}

// export const addBook = async ({ body }: Request, res: Response, next: NextFunction) => {
//     try {
//         const book: IBook = {
//             title: body.title,
//             author: body.author,
//             published_on: body.published_on,
//             isbn: body.isbn,
//             genre: body.genre,
//             price: body.price,
//             description: body.description,
//             qty: body.qty || 0
//         }
//         const conn = await getConnection()
//         conn.query(
//             'INSERT INTO books (title, author, published_on, isbn, genre, price, description, qty) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
//             [book.title, book.author, book.published_on, book.isbn, book.genre, book.price, book.description, book.qty],
//             (err, results: any) => {
//                 conn.release();
//                 if (err) {
//                     console.error('Error inserting book:', err);
//                     throw new Error("Error inserting book");
//                 } else {
//                     console.log('New book inserted successfully:', results);
//                     const result = {
//                         message: 'Book Added Successfully',
//                         data: {
//                             book_id: results[0].insertId,
//                         },
//                         payload: body,
//                     };
//                     res.status(201).json(result);
//                     res.end();
//                 }

//             }
//         );

//     } catch (error) {
//         next(error)
//     }
// }
