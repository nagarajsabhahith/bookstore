// src/modules/books/router.spec.ts

import request from 'supertest';
import express, { Express } from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import router from './router';

const app: Express = express();

app.use(bodyParser.json());

app.use('/api/books', router);

let server: http.Server;

beforeAll(async () => {
    server = app.listen();
});

afterAll(async () => {
    await new Promise<void>((resolve, reject) => {
        server.close((err?: any) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    })
});

describe('Books Router', () => {
    test('POST /api/books with valid parameters should return 200 OK', async () => {
        const validPayload = {
            search: "dan",
            page: 1,
            sort: "title",
            order: "desc",
            out_stock: true,
            published: {
                from: "1973-03-31T18:30:00.000Z",
                to: "2024-02-20T18:30:00.000Z"
            }
        };

        const response = await request(server)
            .post('/api/books')
            .send(validPayload)
            .expect(200);

        expect(response.body).toBeDefined();
        expect(response.body.data).toBeDefined();
        expect(response.body.data.books).toBeInstanceOf(Array);
    });
});
