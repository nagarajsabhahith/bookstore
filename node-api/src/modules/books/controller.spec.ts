// src/modules/books/controller.spec.ts

import { searchBook } from './controller';
import { getConnection } from '../../utils/db';

jest.mock('../../utils/db');

describe('searchBook', () => {
    // Test case for successful search
    it('should return search results when the search query is valid', async () => {
        // Mocked database connection
        const mockConnection = {
            query: jest.fn().mockImplementation((query: string, params: any[], callback: Function) => {
                if (query.includes('SELECT COUNT(*)')) {
                    callback(null, [{ total: 2 }]);
                } else {
                    callback(null, [{ id: 1, title: 'Book 1', author: 'Author 1', genre: 'Genre 1', qty: 10 }]);
                }
            }),
            release: jest.fn(),
        };

        // Mock getConnection to return the mock connection
        (getConnection as jest.Mock).mockResolvedValue(mockConnection);


        const result = await searchBook(
            'dan',
            5,
            1,
            false,
            'title',
            'asc',
            '1973-03-31T18:30:00.000Z',
            '2024-02-20T18:30:00.000Z'
        );

        // Assertions
        expect(getConnection).toHaveBeenCalledTimes(1);
        expect(mockConnection.query).toHaveBeenCalledTimes(2);
        expect(result.totalResults).toBe(2);
        expect(result.totalPages).toBe(1);
        expect(result.currentPage).toBe(1);
        expect(result.books).toHaveLength(1);
        expect(result.books[0]).toEqual({ id: 1, title: 'Book 1', author: 'Author 1', genre: 'Genre 1', qty: 10 });
    });
});
