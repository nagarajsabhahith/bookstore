// src/modules/books/params.ts

import Joi from 'joi';

export const addBookSchema = Joi.object({
    title: Joi.string().required().min(3).max(255).label("Title"),
    author: Joi.string().required().min(3).max(100).label("Author"),
    published_on: Joi.date().less('now').required().label("Published On"),
    isbn: Joi.string().pattern(/^\d+$/).required().min(3).max(20).label("ISBN"),
    genre: Joi.string().required().min(3).max(100).label("Genre"),
    price: Joi.number().required().label("Price"),
    description: Joi.string().required().min(1).max(65535).label("Description"),
    qty: Joi.number().integer().optional().label("Qty")
});


export const searchBooksSchema = Joi.object({
    search: Joi.string().optional().allow('').label("Search"),
    sort: Joi.string().optional().allow('').regex(/^(title|author|published_on)$/).label('Sort By'),
    order: Joi.string().optional().allow('').regex(/^(asc|desc)$/).label('Order'),
    page: Joi.number().optional().label('Page'),
    out_stock: Joi.boolean().default(false),
    published: Joi.object({
        from: Joi.date().required().allow(null),
        to: Joi.date().required().allow(null).greater(Joi.ref('from'))
    }).allow(null)
});
