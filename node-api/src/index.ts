// src/index.ts

import dotenv from 'dotenv';
dotenv.config()

import express, { Request, Response } from 'express';
import { validateConnection } from './utils/db';
import { urlencoded, json } from 'body-parser';
import compression from 'compression';
import cors from 'cors';

import booksRouter from './modules/books/router';
import errorHandler from './middlewares/error-handler';

const app = express();
const port = 3300;

app.use(urlencoded({
    extended: true,
}));
app.use(json());
app.use(cors());

const handleCompression = (req: Request, res: Response) => {
    if (req.headers['x-no-compression']) {
        return false;
    }
    return compression.filter(req, res);
};
// Compression for response body size
// By default all response body will be compressed
// If user don't need compression then user have to pass request header 'x-no-compression' with 'true'
app.use(compression({
    filter: handleCompression,
    threshold: 0,
}));

app.use('/api/books', booksRouter)

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.use(errorHandler);

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
    try {
        validateConnection()
    } catch (error) {
        console.error(error)
    }
});
