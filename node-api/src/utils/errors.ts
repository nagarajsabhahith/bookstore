// src/utils/errors.ts

export class HttpError extends Error {
    status: number;

    constructor(status: number, message: string) {
        super(message);
        this.status = status;
        Error.captureStackTrace(this, this.constructor);
    }
}

export interface IErrorObj {
    error: string;
    context?: any;
    payload?: any;
}

export class BadRequest extends HttpError {
    constructor(message: IErrorObj = { error: 'Bad Request!' }) {
        super(400, (message instanceof Object) ? JSON.stringify(message) : message);
    }
}