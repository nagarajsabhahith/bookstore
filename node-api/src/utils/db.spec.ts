// src/utils/db.spec.ts

import pool, { validateConnection, getConnection } from './db';

jest.mock('mysql2', () => {
    const mockPool = {
        getConnection: jest.fn()
    };
    return {
        createPool: jest.fn(() => mockPool)
    };
});

describe('DB Connection', () => {
    // Test for validateConnection function
    describe('validateConnection', () => {
        it('should log connection message if successful', async () => {
            const mockConn = { threadId: 1, release: jest.fn() };
            (pool.getConnection as jest.Mock).mockImplementationOnce((callback) => {
                callback(null, mockConn);
            });
            console.log = jest.fn();
            await validateConnection();

            expect(console.log).toHaveBeenCalledWith('Connected to MySQL as id 1');
            expect(mockConn.release).toHaveBeenCalled();
        });

        it('should throw an error if connection fails', async () => {
            const mockError = new Error('Connection error');
            (pool.getConnection as jest.Mock).mockImplementationOnce((callback) => {
                callback(mockError);
            });
            console.error = jest.fn();
            const expectedErrorMessage = "Error connecting to MySQL";

            await expect(validateConnection()).rejects.toThrow(expectedErrorMessage);
            expect(console.error).toHaveBeenCalledWith(mockError);
        });
    });

    // Test for getConnection function
    describe('getConnection', () => {
        it('should resolve with a connection if successful', async () => {
            const mockConn = { dummyConnection: true };
            (pool.getConnection as jest.Mock).mockImplementationOnce((callback) => {
                callback(null, mockConn);
            });
            const connection = await getConnection();
            expect(connection).toEqual(mockConn);
        });

        it('should reject with an error if connection fails', async () => {
            const mockError = new Error('Connection error');
            (pool.getConnection as jest.Mock).mockImplementationOnce((callback) => {
                callback(mockError);
            });
            await expect(getConnection()).rejects.toThrow(mockError);
        });
    });
});