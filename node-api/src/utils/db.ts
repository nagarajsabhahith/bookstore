// src/utils/db.ts

import mysql, { PoolOptions, PoolConnection } from 'mysql2';

const access: PoolOptions = {
    user: process.env.DB_USER || '',
    password: process.env.DB_PASSWORD || '',
    host: process.env.DB_HOST || '',
    port: parseInt(<string>process.env.DB_PORT) || 3306,
    database: process.env.DB_DATABASE || '',
};


const pool = mysql.createPool(access);


export const validateConnection = async () => {
    pool.getConnection((err, conn) => {
        if (err) {
            console.error(err);
            throw new Error("Error connecting to MySQL");
        }
        console.log('Connected to MySQL as id ' + conn.threadId);
        conn.release();
    });
}

export const getConnection = async (): Promise<PoolConnection> => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, conn) => {
            if (err) {
                console.error(err);
                reject(err)
            }
            resolve(conn)
        });
    })
}


export default pool;