module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    moduleFileExtensions: ['ts', 'js'],
    testMatch: ['**/*.spec.ts', '**/*.test.ts'],
    setupFiles: ['./testSetup.ts']
};