// src/pages/HomePage.tsx

import React from 'react';
import { Container, Typography, List, ListItem, ListItemText, Grid } from '@mui/material'; // Import Material-UI components

interface BookProps {
    id: string;
    title: string;
    author: string;
    isbn: string;
    description: string;
    publishedOn: string;
    imageUrl: string;
    genre: string;
}

const Book: React.FC<BookProps> = ({ id, title, author, isbn, description, publishedOn, genre, imageUrl }) => {
    return (
        <div style={{ display: 'flex', marginBottom: '20px' }}>
            <img src={imageUrl} alt={title} style={{ width: '150px', marginRight: '20px' }} />
            <div>
                <h2>{title}</h2>
                <p><strong>Author:</strong> {author}</p>
                <p><strong>ISBN:</strong> {isbn}</p>
                <p><strong>Description:</strong> {description}</p>
                <p><strong>Published on:</strong> {publishedOn}</p>
                <p><strong>Genre:</strong> {genre}</p>
            </div>
        </div>
    );
};

export default Book;
