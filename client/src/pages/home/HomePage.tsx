// src/pages/HomePage.tsx

import React, { useState, useEffect } from 'react';
import {
    Container, Typography, List, ListItem, ListItemText, Grid, TextField, Pagination,
    FormControl, InputLabel, MenuItem, FormLabel, FormGroup, Switch, FormControlLabel, Button, ListSubheader
} from '@mui/material'; // Import Material-UI components
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Book from '../../components/book';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';

import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { Dayjs } from 'dayjs';

interface IBook {
    id: string;
    title: string;
    author: string;
    isbn: string;
    description: string;
    published_on: string;
    imageUrl?: string;
    genre: string;
}

const HomePage = () => {
    const [data, setData] = useState(null);
    const [books, setBooks] = useState<IBook[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [error, setError] = useState<Error | null>(null);
    const [name, setName] = useState('');
    const [sort, setSort] = useState('');
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(10);
    const [totalResults, setTotalResults] = useState<number>();
    const [outStock, setOutStock] = useState(false);

    const [startDate, setStartDate] = useState<Dayjs | null>(null);
    const [endDate, setEndDate] = useState<Dayjs | null>(null);
    const [published, setPublished] = useState<{ from: Dayjs | null, to: Dayjs | null }>({
        from: null,
        to: null,
    })



    const sortChange = (event: SelectChangeEvent) => {
        setSort(event.target.value as string);
    }

    const paginationChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    }

    const searchFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        console.log(event.target.value)
        setName(event.target.value);
    }

    const handleOutofStock = (event: React.ChangeEvent<HTMLInputElement>) => {
        setOutStock(event.target.checked);
    };

    const handlePublishedRange = () => {
        setPublished({
            from: startDate,
            to: endDate
        })
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                let sortBy = '';
                let orderBy = '';
                if (sort !== '') {
                    [sortBy, orderBy] = sort.split('-')
                }
                const response = await fetch('http://localhost:3300/api/books', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        search: name,
                        page: page,
                        sort: sortBy,
                        order: orderBy,
                        out_stock: outStock,
                        published: published
                    })
                });
                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }
                const result = await response.json();
                console.log(result)
                setBooks(result.data.books);
                setData(result.data);
                setTotalPages(result.data.totalPages)
                setTotalResults(result.data.totalResults)
            } catch (error: unknown) {
                if (error instanceof Error) {
                    setError(error);
                }
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, [name, page, outStock, sort, published]);

    if (isLoading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>Error: {error.message}</div>;
    }

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <div style={{ paddingTop: '40px', paddingBottom: '40px' }}>
                <Container maxWidth="md">
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography variant="h4" component="h4">
                                Book Store
                            </Typography>
                        </Grid>
                        <Grid item xs={4}>
                            <List>
                                <ListSubheader component="div">
                                    Filters
                                </ListSubheader>
                                <ListItem>
                                    <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">Sort By</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={sort}
                                            label="Sort By"
                                            onChange={sortChange}
                                        >
                                            <MenuItem value={'title-asc'}>Title  <ArrowUpwardIcon /> </MenuItem>
                                            <MenuItem value={'title-desc'}>Title <ArrowDownwardIcon /> </MenuItem>
                                            <MenuItem value={'author-asc'}>Author <ArrowUpwardIcon /></MenuItem>
                                            <MenuItem value={'author-desc'}>Author <ArrowDownwardIcon /></MenuItem>
                                            <MenuItem value={'published_on-asc'}>Publication Date <ArrowUpwardIcon /></MenuItem>
                                            <MenuItem value={'published_on-desc'}>Publication Date <ArrowDownwardIcon /></MenuItem>
                                        </Select>
                                    </FormControl>
                                </ListItem>
                                <ListItem>
                                    <FormControl fullWidth component="fieldset">
                                        <FormLabel component="legend">Availability</FormLabel>
                                        <FormGroup aria-label="position" row>
                                            <FormControlLabel
                                                value="start"
                                                control={<Switch checked={outStock}
                                                    onChange={handleOutofStock} color="primary" />}
                                                label="Include out of stock"
                                                labelPlacement="start"
                                            />
                                        </FormGroup>
                                    </FormControl>
                                </ListItem>
                                <ListItem>
                                    <DemoContainer components={['DatePicker']}>
                                        <DatePicker
                                            label='published from'
                                            views={['year', 'month', 'day']}
                                            value={startDate}
                                            maxDate={endDate}
                                            onChange={(value) => setStartDate(value)}
                                        />
                                    </DemoContainer>
                                </ListItem>
                                <ListItem>
                                    <DemoContainer components={['DatePicker']}>
                                        <DatePicker
                                            label='published to'
                                            views={['year', 'month', 'day']}
                                            value={endDate}
                                            minDate={startDate}
                                            onChange={(value) => setEndDate(value)}
                                        />
                                    </DemoContainer>
                                </ListItem>
                                <ListItem>
                                    <Button fullWidth variant="outlined" onClick={handlePublishedRange}>Apply Published Range</Button>
                                </ListItem>
                            </List>
                        </Grid>
                        <Grid item xs={8}>
                            <List>
                                <ListItem>
                                    <TextField fullWidth
                                        id="outlined-basic"
                                        label="Search"
                                        variant="outlined"
                                        helperText="Search by Title, Author, Genre"
                                        value={name}
                                        onChange={searchFieldChange}
                                    />

                                </ListItem>
                                <ListItem>
                                    <Typography variant="body1" component="span">
                                        ({totalResults}) books found
                                    </Typography>
                                </ListItem>
                                {books.map(book => (
                                    <ListItem key={book.id}>
                                        <Book
                                            id={book.id}
                                            title={book.title}
                                            author={book.author}
                                            isbn={book.isbn}
                                            description={book.description}
                                            publishedOn={book.published_on}
                                            genre={book.genre}
                                            imageUrl="https://www.marytribble.com/wp-content/uploads/2020/12/book-cover-placeholder.png"
                                        />
                                    </ListItem>
                                ))}
                                <ListItem>
                                    <Pagination count={totalPages} page={page} onChange={paginationChange} variant="outlined" color="primary" />
                                </ListItem>
                            </List>

                        </Grid>
                    </Grid>
                </Container>
            </div>
        </LocalizationProvider>

    );
};

export default HomePage;
