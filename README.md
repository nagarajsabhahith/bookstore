# Getting Started

## Prerequisites
    - Docker
    - DBeaver
    - NodeJS
    - Typescript
    - React

## Get this Repository
Clone this repository
```
git clone https://gitlab.com/nagarajsabhahith/bookstore.git
```
Folder "bookstore" contains both frontend & backend services. 

Folder "bookstore" is service root folder

```
bookstore/client/ - contains react app to show the search functionality
bookstore/node-api/ - contains nodejs service 
```

## MySQL Database Creation, Connection

#### Step 1: Setup MySQL instance
(Used MySQL Docker container for this task for handle MySQL database)
```
docker run --name book-store -e MYSQL_ROOT_PASSWORD=mypassword -p 3306:3306 -d mysql:latest --bind-address=0.0.0.0
```
This creates "book-store" container with password mentioned for the "root" user and enables the host & port.

#### Step 2: Connect & Create Database
Connect to any database management tool using the connection details, (I have used "DBeaver" tool to connect the database).

Connection Details
```
host: localhost
port: 3306
user: root
password: mypassword
```

Create Database & Table
```
CREATE DATABASE book_store;

USE book_store;

CREATE TABLE books (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    author VARCHAR(100) NOT NULL,
    published_on DATE,
    isbn VARCHAR(20) UNIQUE,
    genre VARCHAR(50),
    language VARCHAR(50),
    price DECIMAL(10, 2),
    description TEXT,
    qty INT NOT NULL DEFAULT 0,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

UPDATE books
SET published_on = STR_TO_DATE(published_on, '%Y-%m-%d');

```
Execute above commands to create database and table schema.

#### Step 3: Load Sample Book Data
You can found some sample books data here in this path.
```
sample-books.csv
```

Import this sample data into "books" table

## Setup Node Api

Go to the node-api folder from root folder
```
cd node-api
```
Run npm install command to install the dependencey packages
```
npm install
```
Check the .env file if any environment configuration changes. 

Start the server
```
npm run start
```
Server will be start running @ port 3300. http://localhost:3300/

Supported endpoint
```
POST http://localhost:3300/api/books 

Headers:
    Content-Type: application/json;

Payload: JSON Body

{
    "search": "dan",
    "page": 1,
    "sort": "title",
    "order": "asc",
    "out_stock": false,
    "published": {
        "from":"1973-03-31T18:30:00.000Z",
        "to": "2024-02-20T18:30:00.000Z"
    }
}

```

For unit & integration testing
```
npm run test
npm run test:coverage
```


## Setup Client
Go to the client folder from root folder

```
cd client
```
Run npm install command to install the dependencey packages
```
npm install
```

Start the client service
```
npm run start
```
Client service starts @ port 3000 http://localhost:3000/

Open the http://localhost:3000/ and check the **Search Feature Implementation** demo


## Machine & Other Configuration
- Mac OS 14.3.1
- Node version v20.11.1
- npm version 10.4.0
- React ^18.2.0
- React MUI - v5.15.11
- MySQL Docker Image - mysql:latest


### Thank You for the Task
Nagaraja Sabhahith
